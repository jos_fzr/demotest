package com.example.testapp.parent

import com.example.testapp.BaseFragment
import com.example.testapp.parent.childFragment.IChildRouter

/**
 * @author jhaddad on 6/27/18.
 */
class ParentFragment : BaseFragment<IParentRouter>(), IChildRouter {

    override fun doChildRouterStuff() {
        router?.doParentRouterStuff()
    }
}