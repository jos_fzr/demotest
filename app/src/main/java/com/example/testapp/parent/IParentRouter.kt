package com.example.testapp.parent

/**
 * @author jhaddad on 6/27/18.
 */
interface IParentRouter {
    fun doParentRouterStuff()
}