package com.example.testapp.parent.childFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testapp.*

/**
 * @author jhaddad on 6/27/18.
 */
class ChildFragment : BaseFragment<IChildRouter>(), IAdapterEventsListener, IUsersDataProvider {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        router?.doChildRouterStuff()

        val adapter = UsersAdapter(this, this)

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onUserClicked(user: User?) {
        //val user = interactor.getUserbyId(user.id)
        // adapter.updateUser(user)
    }

    override fun provideUsers(): List<User> {
        return ArrayList()
    }
}