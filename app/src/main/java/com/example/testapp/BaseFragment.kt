package com.example.testapp

import android.content.Context
import android.support.v4.app.Fragment

/**
 * @author jhaddad on 6/27/18.
 */
open class BaseFragment<Router> : Fragment() {

    var router: Router? = null
        private set

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (parentFragment != null) {
            router = parentFragment as Router
        } else {
            router = activity as Router
        }
    }
}