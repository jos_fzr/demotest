package com.example.testapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import com.example.testapp.databinding.ActivityMainBinding
import com.example.testapp.parent.IParentRouter

class MainActivity : AppCompatActivity(), IParentRouter {

    var mBinding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val layoutInflater = LayoutInflater.from(this)
        mBinding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(mBinding?.root)

        val user = User(1, "")

        mBinding?.user = user
    }

    override fun doParentRouterStuff() {

    }
}
