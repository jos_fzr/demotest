package com.example.testapp

/**
 * @author jhaddad on 6/25/18.
 */
data class User(val id: Long, val name: String)