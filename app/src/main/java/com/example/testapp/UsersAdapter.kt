package com.example.testapp

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup

/**
 * @author jhaddad on 6/27/18.
 */

interface IUsersDataProvider {
    fun provideUsers() : List<User>
}

interface IAdapterEventsListener {
    fun onUserClicked(user : User?)
}

interface IViewHolderListener {
    fun onViewholderClick(position: Int)
}

class UserViewHolder : RecyclerView.ViewHolder {

    companion object {

        fun newInstance(viewGroup: ViewGroup, clickListene: IViewHolderListener) : UserViewHolder {
            return UserViewHolder(null, clickListene)
        }
    }

    private constructor(itemView: View?, listener : IViewHolderListener) : super(itemView) {
        itemView?.setOnClickListener { listener.onViewholderClick(adapterPosition) }
    }

    fun bind(user: User?) {

    }
}

class UsersAdapter(private val dataProvider: IUsersDataProvider,
                   private val eventsListener: IAdapterEventsListener)
    : RecyclerView.Adapter<UserViewHolder>(), IViewHolderListener {

    private var mUsers : MutableList<User>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder.newInstance(parent, this)
    }

    override fun getItemCount(): Int {
        return mUsers?.size ?: 0
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(mUsers?.get(position))
    }

    override fun onViewholderClick(position: Int) {
        eventsListener.onUserClicked(mUsers?.get(position))
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        mUsers = dataProvider.provideUsers().toMutableList()
        notifyDataSetChanged()
    }

    fun updateUser(user: User) {
        val index = mUsers?.withIndex()?.firstOrNull { it.value.id == user.id }?.index

        if (index ?: -1 > 0) {
            mUsers?.removeAt(index!!)
            mUsers?.set(index!!, user)
            notifyItemChanged(index!!)
        }
    }
}